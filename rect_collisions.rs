use raylib::prelude::*;
use raylib::consts::KeyboardKey::*;

const SCREEN_WIDTH: f32 = 640.0;
const SCREEN_HEIGHT: f32 = 480.0;

fn main() {
	let (mut rl, thread) = raylib::init()
		.size(SCREEN_WIDTH as i32, SCREEN_HEIGHT as i32)
		.title("Random values")
		.vsync()
		.build();

	let mut rect1 = Rectangle::new(0.0, 0.0, 50.0, 50.0);

	let mut rect2 = Rectangle::new(
		SCREEN_WIDTH - 50.0,
		SCREEN_HEIGHT - 50.0,
		50.0,
		50.0
	);

	let speed = 150.0;
	let mut collision: bool;

	while !rl.window_should_close() {
		/* --- UPDATE --- */
		let dt = rl.get_frame_time();

		if rl.is_key_down(KEY_D) { rect1.x += speed * dt; }
		if rl.is_key_down(KEY_S) { rect1.y += speed * dt; }
		if rl.is_key_down(KEY_A) { rect1.x -= speed * dt; }
		if rl.is_key_down(KEY_W) { rect1.y -= speed * dt; }

		if rl.is_key_down(KEY_RIGHT) { rect2.x += speed * dt; }
		if rl.is_key_down(KEY_DOWN)  { rect2.y += speed * dt; }
		if rl.is_key_down(KEY_LEFT)  { rect2.x -= speed * dt; }
		if rl.is_key_down(KEY_UP)    { rect2.y -= speed * dt; }

		collision = rect1.check_collision_recs(&rect2);



		/* --- DRAW --- */
		let mut d = rl.begin_drawing(&thread);
		d.clear_background(Color::BLACK);

		d.draw_rectangle_rec(&rect1, Color::GREEN);
		d.draw_rectangle_rec(&rect2, Color::YELLOW);

		if collision {
			d.draw_text(
				"Collision!",
				100,
				40,
				32,
				Color::RAYWHITE
			);
		}

		d.draw_fps(10, 10);
	}
}
