use raylib::prelude::*;
use raylib::consts::MouseButton::*;

const SCREEN_WIDTH: f32 = 640.0;
const SCREEN_HEIGHT: f32 = 480.0;

struct Ball {
	position: Vector2,
	speed: f32,
	radius: f32,
	color: Color
}

fn main() {
	let (mut rl, thread) = raylib::init()
		.size(SCREEN_WIDTH as i32, SCREEN_HEIGHT as i32)
		.title("Mouse input")
		.vsync()
		.build();

	let mut ball = Ball {
		position: Vector2::new(SCREEN_WIDTH / 2.0, SCREEN_HEIGHT / 2.0),
		speed: 3.0,
		radius: 40.0,
		color: Color::GREEN
	};

	while !rl.window_should_close() {
		/* --- UPDATE --- */
		if rl.is_mouse_button_down(MOUSE_LEFT_BUTTON) {
			ball.position = ball.position.lerp(
				rl.get_mouse_position(),
				0.025
			);
		}

		if rl.is_mouse_button_pressed(MOUSE_RIGHT_BUTTON) {
			ball.position = rl.get_mouse_position();

			if ball.color == Color::GREEN {
				ball.color = Color::YELLOW;
			} else {
				ball.color = Color::GREEN;
			}
		}


		/* --- DRAW --- */
		let mut d = rl.begin_drawing(&thread);

		d.clear_background(Color::BLACK);
		d.draw_circle_v(ball.position, ball.radius, ball.color);
	}
}
