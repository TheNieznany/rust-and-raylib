use raylib::prelude::*;

fn main() {
	let (mut rl, thread) = raylib::init()
		.size(640, 480)
		.title("Hello, World!")
		.vsync()
		.build();

	while !rl.window_should_close() {
		let mut d = rl.begin_drawing(&thread);

		d.clear_background(Color::BLACK);
		d.draw_text("Hello, World!", 12, 12, 24, Color::WHITE);
	}
}
