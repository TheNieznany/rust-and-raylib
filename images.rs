use raylib::prelude::*;
use raylib::consts::KeyboardKey::*;

const SCREEN_WIDTH: f32 = 640.0;
const SCREEN_HEIGHT: f32 = 480.0;

pub struct Player {
	pub position: Vector2,
	pub sprite: Texture2D,
}

impl Player {
	pub fn new(
		rl: &mut RaylibHandle,
		thread: &RaylibThread,
		filepath: &'static str
	) -> Self {
		Self {
			position: Vector2::zero(),
			sprite: rl.load_texture(thread, filepath).unwrap(),
		}
	}

	pub fn draw(&self, d: &mut RaylibDrawHandle) {
		d.draw_texture_v(&self.sprite, &self.position, Color::WHITE);
	}
}

fn main() {
	let (mut rl, thread) = raylib::init()
		.size(SCREEN_WIDTH as i32, SCREEN_HEIGHT as i32)
		.title("Rust & raylib #8 - Loading and rendering images")
		.vsync()
		.build();

	let image = rl.load_texture(&thread, "images/x.png").unwrap();

	let mut player = Player::new(&mut rl, &thread, "images/x.png");

	while !rl.window_should_close() {
		/* ----- UPDATE ----- */
		if rl.is_key_down(KEY_RIGHT) { player.position.x += 3.0; }
		if rl.is_key_down(KEY_DOWN) { player.position.y += 3.0; }
		if rl.is_key_down(KEY_LEFT) { player.position.x -= 3.0; }
		if rl.is_key_down(KEY_UP) { player.position.y -= 3.0; }

		/* ----- DRAW ----- */
		let mut d = rl.begin_drawing(&thread);
		d.clear_background(Color::BLACK);

		d.draw_texture_v(&image, Vector2::new(15.0, 15.0), Color::WHITE);

		d.draw_texture_rec(
			&image,
			Rectangle::new(0.0, 0.0, 92.0, 92.0),
			Vector2::new(200.0, 70.0),
			Color::WHITE
		);

		player.draw(&mut d);
	}
}
